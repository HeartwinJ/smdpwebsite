import { Component, OnInit } from '@angular/core';
declare const Waypoint: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var waypointHome = new Waypoint({
      element: document.getElementById('waypointHome'),
      handler: function() {
        console.log('Home waypoint triggered');
        document.getElementById('aboutLbl').classList.remove('active');
        document.getElementById('visionLbl').classList.remove('active');
        document.getElementById('researchLbl').classList.remove('active');
      }
    });
    
    var waypointAbout = new Waypoint({
      element: document.getElementById('waypointAbout'),
      handler: function() {
        console.log('About waypoint triggered');
        document.getElementById('aboutLbl').classList.add('active');
        document.getElementById('visionLbl').classList.remove('active');
        document.getElementById('researchLbl').classList.remove('active');
      }
    });
    
    var waypointVision = new Waypoint({
      element: document.getElementById('waypointVision'),
      handler: function() {
        console.log('Vision waypoint triggered');
        document.getElementById('aboutLbl').classList.remove('active');
        document.getElementById('visionLbl').classList.add('active');
        document.getElementById('researchLbl').classList.remove('active');
      }
    });

    var waypointResearch = new Waypoint({
      element: document.getElementById('waypointResearch'),
      handler: function() {
        console.log('Research waypoint triggered');
        document.getElementById('aboutLbl').classList.remove('active');
        document.getElementById('visionLbl').classList.remove('active');
        document.getElementById('researchLbl').classList.add('active');
      }
    });
  }

  aboutClick(){
    document.getElementById('waypointAbout').scrollIntoView(true);
  }

  visionClick(){
    document.getElementById('waypointVision').scrollIntoView(true);
  }

  researchClick(){
    document.getElementById('waypointResearch').scrollIntoView(true);
  }

}
