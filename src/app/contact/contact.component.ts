import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }    
  content=[
    {
      name: 'Dr. G. Lakshmi Sutha',
      designation: 'Assistant Professor & Head (ECE) Chief Investigator (CI) SMDP-C2SD Project',
      number: '+91 4368 265235 (Ext: 220)',
      mail: 'lakshmi@nitpy.ac.in'
    },
    {
      name: 'Dr. Malaya Kumar Nath',
      designation: 'Assistant Professor, Co-Chief-Investigator (Co-CI) SMDP-C2SD Project',
      number: '+91 4368 265235 (Ext: 222)',
      mail: 'malaya.nath@nitpy.ac.in'
    },
    {
      name: 'Dr. Aniruddha Kanhe',
      designation: 'Assistant Professor ,Co-Chief-Investigator (Co-CI) SMDP-C2SD Project',
      number: '+91 4368 265235 (Ext: 223)',
      mail: 'aniruddhakanhe@nitpy.ac.in'
    },
    {
      name: 'Mr. Nettimi Satya Sai Srinivas',
      designation: 'Research Associate SMDP-C2SD Project',
      number: '+91 4368 265235 (Ext: 243)',
      mail: 'satya_srinivasnettimi@live.com'
    },
    {
      name: 'Mr. N. Sugan',
      designation: 'Lab Engineer SMDP-C2SD Project',
      number: '+91 4368 265235 (Ext: 243)',
      mail: 'sugannece@gmail.com'
    },
  ]
}
