import { Component, OnInit } from '@angular/core';
declare const Waypoint: any;

@Component({
  selector: 'app-teachings',
  templateUrl: './teachings.component.html',
  styleUrls: ['./teachings.component.scss']
})
export class TeachingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var waypointCourses = new Waypoint({
      element: document.getElementById('waypointCourses'),
      handler: function() {
        console.log('Courses waypoint triggered');
        document.getElementById('coursesLbl').classList.add('active');
        document.getElementById('labsLbl').classList.remove('active');
      }
    });

    var waypointLabs = new Waypoint({
      element: document.getElementById('waypointLabs'),
      handler: function() {
        console.log('Labs waypoint triggered');
        document.getElementById('coursesLbl').classList.remove('active');
        document.getElementById('labsLbl').classList.add('active');
      }
    });
  }

  coursesClick(){
    document.getElementById('waypointCourses').scrollIntoView(true);
  }

  labsClick(){
    document.getElementById('waypointLabs').scrollIntoView(true);
  }

}
