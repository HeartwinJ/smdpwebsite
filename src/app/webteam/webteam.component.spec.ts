import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebteamComponent } from './webteam.component';

describe('WebteamComponent', () => {
  let component: WebteamComponent;
  let fixture: ComponentFixture<WebteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
