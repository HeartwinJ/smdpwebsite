import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ResourcesComponent } from './resources/resources.component';
import { PeopleComponent } from './people/people.component';
import { TeachingsComponent } from './teachings/teachings.component';
import { ContactComponent } from './contact/contact.component';
import { TrainingsComponent } from './trainings/trainings.component';
import { PublicationsComponent } from './publications/publications.component';
import { WebteamComponent } from './webteam/webteam.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ResourcesComponent,
    PeopleComponent,
    TeachingsComponent,
    ContactComponent,
    TrainingsComponent,
    PublicationsComponent,
    WebteamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
