import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  faculty=[
    {
      pic:'assets/images/faculty/fac001.jpg',
      name:'Dr. G.Lakshmi Sutha',
      qualification:'Ph.D. NTU-Singapore',
      designation:'Assistant Professor & Head (ECE), Chief Investigator (CI) SMDP-C2SD Project',
      research:'Remote Sensing, Climate and Weather Studies, Signal processing, Speech processing and Image processing.',
      address:'Room No: FF-R2, Department of ECE, Science Block, NIT-Puducherry, Karaikal, India – 609 609. ',
      mail:'lakshmi@nitpy.ac.in'
    },
    {
      pic:'assets/images/faculty/fac002.jpg',
      name:'Dr. Malaya Kumar Nath',
      qualification:'Ph.D. IIT-Guwahati',
      designation:'Assistant Professor, Co-Chief Investigator (Co-CI) SMDP-C2SD Project',
      research:'Image Processing, Signal Processing, Pattern Recognition, VLSI Image Processing.',
      address:'Room No: FF-R4, Department of ECE, Science Block, NIT-Puducherry, Karaikal, India – 609 609.',
      mail:'malaya.nath@nitpy.ac.in'
    },
    {
      pic:'assets/images/faculty/fac003.jpg',
      name:'Dr. Aniruddha Kanhe',
      qualification:'Ph.D. NIT Puducherry',
      designation:'Assistant Professor, Co-Chief Investigator (Co-CI) SMDP-C2SD Project',
      research:'Audio Steganography and Watermarking, VLSI Signal Processing.',
      address:'Room No: FF-R5, Department of ECE, Science Block, NIT-Puducherry, Karaikal, India – 609 609.',
      mail:'aniruddhakanhe@nitpy.ac.in'
    },
    {
      pic:'assets/images/faculty/fac004.jpg',
      name:'Dr. Narendran Rajagopalan',
      qualification:'Ph.D. (CSE) - NIT Trichy',
      designation:'Assistant Professor, Member SMDP-C2SD Project',
      research:'Wireless Networks, Internet of Things, Soft Computing',
      address:'Room No: FF-R11, Department of CSE, Science Block, NIT-Puducherry, Karaikal, India – 609 609.',
      mail:'narendran@nitpy.ac.in'
    },
  ];
  staff=[
  {
    pic:'assets/images/faculty/fac005.jpg',
    name:'Mr. Nettimi Satya Sai Srinivas',
    qualification:'Integrated M.E. (Andhra University) Doing Ph. D. under SMDP-C2SD Project ',
    designation:'Research Associate SMDP-C2SD Project',
    research:'Digital Speech Signal Processing, Machine Learning, Digital VLSI System Designs (FPGA & ASIC).',
    mail:'satya_srinivasnettimi@live.com'
  },
  {
    pic:'assets/images/faculty/fac006.jpg',
    name:'Mr. N. Sugan',
    qualification:'M.E. (Anna University) Doing Ph. D. under SMDP-C2SD Project',
    designation:'Lab Engineer SMDP-C2SD Project',
    research:'Digital Speech Signal Processing, Analog and Digital VLSI System Designs (FPGA), Machine Learning.',
    mail:'sugannece@gmail.com'
  },
  {
    pic:'assets/images/faculty/fac007.jpg',
    name:'Mr. Sreesh P R',
    qualification:'M.Tech (NIT Calicut) Doing Part-time Ph.D. – Visvesvarya Scheme (SMDP-C2SD Project)',
    designation: 'Research Scholar',
    research:'Low Power VLSI Design, Analog and Digital VLSI Design, Embedded systems.',
    mail:'sreeshprpnkm@gmail.com'
  },
  {
    pic:'assets/images/faculty/fac008.jpg',
    name:'Mr. Niladri Kar',
    qualification:'B.Tech., P.G.D. (NIELIT-Calicut)',
    designation:'Project Assistant SMDP-C2SD Project',
    research:'Digital VLSI System Designs.',
    
    mail:'niladri1357@gmail.com'
  },
  
]
}
