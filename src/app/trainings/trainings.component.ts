import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss']
})
export class TrainingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  content=[
    {
      type:'IEP',
      topic:'Mixed Signal SoC – From Design to Tape out (Hands-On)',
      date:'04-08 July 2016',
      institute:'IISc-Bangalore'
    },{
      type:'Workshop',
      topic:'Xilinx Training Tools',
      date:'02-10 December, 2016',
      institute:'NIT-Goa'
    },{
      type:'Training',
      topic:'Synopsys EDA Tools Training Program',
      date:'05-10 January, 2017',
      institute:'NIT-Tiruchirappalli'
    },{
      type:'Training',
      topic:'Mentor Graphics Tools Training Program',
      date:'16-19 January, 2017',
      institute:'PSG College of Technology, Coimbatore'
    },{
      type:'Training',
      topic:'Cadence Tools Training Program',
      date:'23-26 January, 2017',
      institute:'PSG College of Technology, Coimbatore'
    },{
      type:'IEP',
      topic:'Analog IC Design',
      date:'29 January – 02 February, 2018',
      institute:'IIT-Madras'
    },{
      type:'IEP',
      topic:'High Level Design to Silicon',
      date:'24-27 February, 2018',
      institute:'IIT-Roorkee'
    },{
      type:'IEP',
      topic:'Chip Tapeout',
      date:'24-28 June, 2019',
      institute:'IISc-Bangalore'
    },{
      type:'IEP',
      topic:'Design Verification and Hardware Security',
      date:'01-05 July, 2019',
      institute:'NIT Rourkela'
    }
  ]
}
