import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ResourcesComponent } from './resources/resources.component';
import { PeopleComponent}from './people/people.component';
import { TeachingsComponent } from './teachings/teachings.component';
import { TrainingsComponent } from'./trainings/trainings.component';
import { PublicationsComponent } from './publications/publications.component';
import { ContactComponent } from './contact/contact.component';
import { WebteamComponent } from './webteam/webteam.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: 'people', component: PeopleComponent},
  { path: 'teachings', component: TeachingsComponent},
  { path: 'trainings-workshops-ieps', component: TrainingsComponent},
  { path: 'publications', component: PublicationsComponent},
  { path: 'contact-us', component: ContactComponent},
  { path: 'webteam', component: WebteamComponent},
  { path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
