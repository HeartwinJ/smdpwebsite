import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  /*Edit the Contact Details in the Footer here*/
  contact = {
    email: "123@nitpy.ac.in",
    website: "nitpy.ac.in",
    phone: "1234567890",
    fax: "1234567890"
  }
}
