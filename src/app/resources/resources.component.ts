import { Component, OnInit } from '@angular/core';
declare const Waypoint: any;

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var waypointSystems = new Waypoint({
      element: document.getElementById('waypointSystems'),
      handler: function() {
        console.log('Systems waypoint triggered');
        document.getElementById('systemsLbl').classList.add('active');
        document.getElementById('softwareLbl').classList.remove('active');
        document.getElementById('hardwareLbl').classList.remove('active');
        document.getElementById('textbookLbl').classList.remove('active');
        document.getElementById('corporaLbl').classList.remove('active');
      }
    });
    
    var waypointSoftware = new Waypoint({
      element: document.getElementById('waypointSoftware'),
      handler: function() {
        console.log('Software waypoint triggered');
        document.getElementById('systemsLbl').classList.remove('active');
        document.getElementById('softwareLbl').classList.add('active');
        document.getElementById('hardwareLbl').classList.remove('active');
        document.getElementById('textbookLbl').classList.remove('active');
        document.getElementById('corporaLbl').classList.remove('active');
      }
    });

    var waypointHardware = new Waypoint({
      element: document.getElementById('waypointHardware'),
      handler: function() {
        console.log('Hardware waypoint triggered');
        document.getElementById('systemsLbl').classList.remove('active');
        document.getElementById('softwareLbl').classList.remove('active');
        document.getElementById('hardwareLbl').classList.add('active');
        document.getElementById('textbookLbl').classList.remove('active');
        document.getElementById('corporaLbl').classList.remove('active');
      }
    });

    var waypointTextbooks = new Waypoint({
      element: document.getElementById('waypointTextbooks'),
      handler: function() {
        console.log('Textbooks waypoint triggered');
        document.getElementById('systemsLbl').classList.remove('active');
        document.getElementById('softwareLbl').classList.remove('active');
        document.getElementById('hardwareLbl').classList.remove('active');
        document.getElementById('textbookLbl').classList.add('active');
        document.getElementById('corporaLbl').classList.remove('active');
      }
    });

    var waypointCorpora = new Waypoint({
      element: document.getElementById('waypointCorpora'),
      handler: function() {
        console.log('Corpora waypoint triggered');
        document.getElementById('systemsLbl').classList.remove('active');
        document.getElementById('softwareLbl').classList.remove('active');
        document.getElementById('hardwareLbl').classList.remove('active');
        document.getElementById('textbookLbl').classList.remove('active');
        document.getElementById('corporaLbl').classList.add('active');
      }
    });
  }
  
  aboutSystems(){
    document.getElementById('waypointSystems').scrollIntoView(true);
  }

  aboutSoftware(){
    document.getElementById('waypointSoftware').scrollIntoView(true);
  }

  aboutHardware(){
    document.getElementById('waypointHardware').scrollIntoView(true);
  }

  aboutTextbook(){
    document.getElementById('waypointTextbooks').scrollIntoView(true);
  }

  aboutCorpora(){
    document.getElementById('waypointCorpora').scrollIntoView(true);
  }

}
